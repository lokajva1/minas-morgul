"""
Array of routes
"""
from django.urls import path
from first_app.views_dir.book_view import BookView
from first_app.views_dir.movie_view import MovieView
from first_app.views_dir.character_view import CharacterView
from first_app.views_dir.home_view import HomeView
from first_app.views_dir.quote_view import QuoteView

from . import views

# routes
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('book/', BookView.as_view(), name='book'),
    path('movie/', MovieView.as_view(), name='movie'),
    path('character/', CharacterView.as_view(), name='character'),
    path('quote/<str:_id>', QuoteView.as_view(), name='quote'),
    path('movie/<str:_id>', MovieView.as_view(), name='movie_detail'),
    path('book/<str:_id>', BookView.as_view(), name='book_detail'),
    path('character/<str:_id>', CharacterView.as_view(), name='character_detail'),
]