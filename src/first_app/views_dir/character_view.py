"""
View thats return list or detail of character
"""
import requests
from django.shortcuts import render

from first_app.views_dir.master_view import MasterView


class CharacterView(MasterView):

    # GET request
    def get(self, request, **kwargs):
        """

        :rtype: object
        :param request: HtmlRequest
        :param kwargs: header parameters
        :return:
        """
        try:
            if len(kwargs) == 0:
                return self._list(request, 'character', 'character')
            else:
                return self.__character_detail(request, kwargs['_id'])
        except:
            return render(request, 'first_app/error/error_page.html', {})

    # detail tab
    def __character_detail(self, request, _id):
        """

        :param request: HtmlRequest
        :param _id: unique id
        :return: rendered page
        :rtype: object
        """
        response_character = requests.get(self.apiUrl + 'character/' + _id, headers=self.headers)
        response_quote = requests.get(self.apiUrl + 'character/' + _id + '/quote', headers=self.headers)

        # map response to chapter tab format
        docs_character = response_character.json()
        docs_quotes = response_quote.json()

        context = self._make_basic_context()

        # html data
        context['character'] = docs_character['docs'][0]
        context['docs'] = docs_quotes['docs']
        return render(request, 'first_app/detail/character_detail.html', context)
