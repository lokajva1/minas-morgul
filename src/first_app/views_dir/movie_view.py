"""
View to render Movies or Movie detail
"""
import requests
from django.shortcuts import render

from first_app.views_dir.master_view import MasterView


class MovieView(MasterView):

    # GET request
    def get(self, request, **kwargs):
        """

        :rtype: object
        :param request: HtmlRequest
        :param kwargs: Html header parameters
        :return:
        """
        try:
            if len(kwargs) == 0:
                return self._list(request, 'movie', 'movie')
            elif len(kwargs) == 1:
                return self.__movie_detail(request, kwargs['_id'])
        except:
            return render(request, 'first_app/error/error_page.html', {})

    # detail tabs
    def __movie_detail(self, request, _id):
        """
        :rtype: object
        :param request: HtmlRequest
        :param _id: unique id
        :return: rendered page
        """
        response = requests.get(self.apiUrl + 'movie/' + _id, headers=self.headers)
        # map response to chapter tab format
        docs = response.json()
        context = self._make_basic_context()

        # change _id to id
        for element in docs['docs']:
            element['id'] = element.pop('_id')

        # html data
        context['movie'] = docs['docs'][0]
        return render(request, 'first_app/detail/movie_detail.html', context)
