"""
View to render character quotes
"""
import requests
from django.shortcuts import render

from first_app.views_dir.master_view import MasterView


class QuoteView(MasterView):

    # GET request
    def get(self, request, **kwargs):
        """

        :rtype: object
        :param request: HtmlRequest
        :param kwargs: Html header
        :return:
        """
        try:
            return self._list(request, 'movie/' + kwargs['_id'] + '/quote', 'quote')
        except:
            return render(request, 'first_app/error/error_page.html', {})
