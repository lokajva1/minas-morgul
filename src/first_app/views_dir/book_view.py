"""
View thats return list or detail of books
"""
import requests
from django.shortcuts import render

from first_app.views_dir.master_view import MasterView


class BookView(MasterView):

    # GET request
    def get(self, request, **kwargs):
        """

        :param request: HtmlRequest
        :param kwargs: headers parameters
        :return:
        """
        try:
            if len(kwargs) == 0:
                return self._list(request, 'book', 'book')
            elif len(kwargs) == 1:
                return self.__book_detail(request, kwargs['_id'])
        except:
            return render(request, 'first_app/error/error_page.html', {})

    # detail tab
    def __book_detail(self, request, _id):
        """

        :param request: HtmlRequest
        :param _id: unique id
        :return: rendered page
        :rtype: object
        """
        response_book = requests.get(self.apiUrl + 'book/' + _id, headers=self.headers)
        response_chapters = requests.get(self.apiUrl + 'book/' + _id + '/chapter', headers=self.headers)
        # map response to chapter tab format
        docs_book = response_book.json()
        docs_chapters = response_chapters.json()
        context = self._make_basic_context()

        # html data
        context['book'] = docs_book['docs'][0]
        context['chapter'] = docs_chapters['docs']
        return render(request, 'first_app/detail/book_detail.html', context)
