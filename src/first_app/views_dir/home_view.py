"""
View to render Home page
"""
import requests
from django.http import HttpResponse
from django.shortcuts import render
from first_app.views_dir.master_view import MasterView


class HomeView(MasterView):

    # GET request
    def get(self, request):
        """

        :rtype: object
        :param request: HtmlRequest
        :return:
        """
        try:
            return render(request, 'first_app/home.html', self._make_basic_context())
        except:
            return render(request, 'first_app/error/error_page.html', {})
