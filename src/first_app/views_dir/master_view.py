""""
Parent class to all views"""
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views import View
import requests
import random as _random


class MasterView(View):

    def __init__(self, **kwargs):
        """

        :rtype: object
        """
        super().__init__(**kwargs)
        # API confiq
        self.apiUrl = 'https://the-one-api.dev/v2/'
        self.headers = {'Authorization': 'Bearer tv5iJ0sb9j88sD3lwz3L'}

    def _make_basic_context(self):
        """

        :rtype: object
        :return:
        """
        # random selection quote
        response_quote = requests.get(self.apiUrl + 'quote', headers=self.headers)
        docs_quote = response_quote.json()
        quote_index = _random.randint(1, len(docs_quote['docs']) - 1)
        quote = docs_quote['docs'][quote_index]
        response_movie = requests.get(self.apiUrl + 'movie/' + quote['movie'], headers=self.headers)
        response_character = requests.get(self.apiUrl + 'character/' + quote['character'], headers=self.headers)
        movie = response_movie.json()
        character = response_character.json()
        context = {
            'r_quote': quote,
            'r_movie': movie['docs'][0],
            'r_character': character['docs'][0]
        }
        return context

    # common method that render list
    # request = WSGI Request
    # endpointUrl = url after basic __apiUrl
    # htmlName = name of the html list file
    def _list(self, request, endpointUrl, htmlName):
        """
        :param request: HtmlRequest
        :param endpointUrl: unique url on API calling
        :param htmlName: name thats is rendered in html file
        :rtype: object
        :return:
        """
        # calling api
        response_json = requests.get(self.apiUrl + endpointUrl, headers=self.headers)
        # map response to movie tab format
        response = response_json.json()

        # empty data
        context = self._make_basic_context()

        # change _id attribute to id
        for element in response['docs']:
            element['id'] = element.pop('_id')

        # paginator
        page = request.GET.get('page', 1)
        paginator = Paginator(response['docs'], 50)

        # load current page
        try:
            docs = paginator.page(page)
        except PageNotAnInteger:
            docs = paginator.page(1)
        except EmptyPage:
            docs = paginator.page(paginator.num_pages)

        # html data
        context['docs'] = docs
        return render(request, 'first_app/list/' + htmlName + '_list.html', context)
